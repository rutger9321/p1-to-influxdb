# P1 To InfluxDB

Reads serial port connected to Dutch Smart Meter (ESMR5.0) with P1 cable.
Tested with: ESMR5.0 and Raspberry Pi 2B+

## Installation

1. Install Python3 and PIP: `sudo apt install python3-pip`
2. Install requirements: `pip install -r requirements.txt`
3. Open `reader.py` and enter configuration for InfluxDB
4. Run `reader.py` to start logging

### Using virtualenv
1. Install virtualenv `pip install virtualenv`
2. Create virtualenv for project: `virtualenv py3 -p /usr/bin/python3`
3. Activate virtualenv: `source py3/bin/activate`
4. Watch your prompt change
5. To deactivate virtualenv type: `deactivate`

## Setup systemd on Raspberry Pi

1. Create new service file: `sudo vim /lib/systemd/system/p1-to-influxdb.service`

2. Add this content:

```
[Unit]
Description=P1 to InfluxDB Service
After=multi-user.target

[Service]
Type=idle
ExecStart=/usr/bin/python3 /home/pi/p1-to-influxdb/reader.py > /home/pi/p1-to-influxdb/reader.log 2>&1
User=pi

[Install]
WantedBy=multi-user.target
```

**Note:** Make sure to update the path to your reader file.

3. Change file rights: `sudo chmod 644 /lib/systemd/system/p1-to-influxdb.service`

4. Reload systemctl: `sudo systemctl daemon-reload`
 
5. Enable the service: `sudo systemctl enable p1-to-influxdb.service`

6. Start the service: `sudo systemctl start p1-to-influxdb.service`

## Troubleshooting

### `PermissionError: [Errno 13] Permission denied: '/var/log/p1-influx.log'`

Make sure the log file exists and is writable by pi.

### `[Errno 2] could not open port /dev/ttyUSB0: [Errno 2] No such file or directory: '/dev/ttyUSB0'`

Make sure USB cable is connected and correct port is addressed (default is correct most of the times).