#!/usr/bin/python3

import logging

from influxdb import InfluxDBClient
from influxdb.client import InfluxDBClientError
from dsmr_parser import telegram_specifications, obis_references
from dsmr_parser.parsers import TelegramParser
from dsmr_parser.clients import SerialReader, SERIAL_SETTINGS_V5

# Configuration
INFLUXHOST = 'rpimon.local'
INFLUXPORT = 8086
INFLUXUSER = ''
INFLUXPASS = ''
INFLUXDBNAME = 'p1'
SERIALDEVICE = '/dev/ttyUSB0'
LOGFILEPATH = '/var/log/p1-influx.log'

# Toggle debug mode
debug = False

# Setup logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('p1-influx')
fh = logging.FileHandler(LOGFILEPATH)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)

logger.info("=====================")
logger.info("P1 to InfluxDB reader")
logger.info("=====================")
logger.info("Source: https://gitlab.com/rutger9321/p1-to-influxdb")
logger.info("Note: output is logged to p1-influx.log")

if (debug == True):
    logger.debug("- DEBUG MODE ACTIVE")

def create_connection():
    try:
        logger.info("Creating InfluxDB connection")
        return InfluxDBClient(INFLUXHOST, INFLUXPORT, INFLUXUSER, INFLUXPASS, INFLUXDBNAME)

    except Exception as e:
        logger.error("No connection available")
        return

def create_database(connection):
    try:
        connection.create_database(INFLUXDBNAME)
        logger.info("- Database created...")
    except InfluxDBClientError:
        loger.error("- Error creating database!")
        return

# Create InfluxDB connection
connection = create_connection()

# Make sure database exists
create_database(connection)

# Create serial reader
serial_reader = SerialReader(
    device=SERIALDEVICE,
    serial_settings=SERIAL_SETTINGS_V5,
    telegram_specification=telegram_specifications.V5
)

logger.info("- Starting logging...")
logger.info("  Use CTRL+C to stop")
# Loop through serial reader read() method
for telegram in serial_reader.read():

    record = []
    data = {
        "measurement": "electricity",
        "tags": {
            "host": "rpip1"
        },
        "time": telegram[obis_references.P1_MESSAGE_TIMESTAMP].value,
        "fields": {
            "current_usage": float(telegram[obis_references.CURRENT_ELECTRICITY_USAGE].value),
            "current_delivery": float(telegram[obis_references.CURRENT_ELECTRICITY_DELIVERY].value),
            "total_usage_low": float(telegram[obis_references.ELECTRICITY_USED_TARIFF_1].value),
            "total_usage_high": float(telegram[obis_references.ELECTRICITY_USED_TARIFF_2].value),
            "total_delivery_low": float(telegram[obis_references.ELECTRICITY_DELIVERED_TARIFF_1].value),
            "total_delivery_high": float(telegram[obis_references.ELECTRICITY_DELIVERED_TARIFF_2].value),
            "active_tariff": float(telegram[obis_references.ELECTRICITY_ACTIVE_TARIFF].value)
        }
    }

    if (debug == True):
        logger.debug(data)
    else:
        record.append(data)
        try:
            connection.write_points(record)
        except Exception:
            logger.error("Unable to write to InfluxDB")

